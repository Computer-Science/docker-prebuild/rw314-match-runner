# Replace with the source docker image
FROM library/ubuntu:20.04

ARG DEBIAN_FRONTEND=noninteractive
ENV TZ=Africa/Johannesburg

RUN apt update && apt install --no-install-recommends --yes build-essential openjdk-14-jdk-headless openmpi-bin libopenmpi-dev && rm -rf /var/lib/apt/lists/*

WORKDIR /workdir/

COPY IngeniousFramework.jar /workdir/
